import React from 'react';
import {shallow} from 'enzyme';
import AppName from "./AppName";

it('renders welcome message with correct appName', () => {
    var appName = shallow(<AppName name="Any name" />);

    expect(appName.text()).toEqual("Welcome to Any name");

});