import React, {Component} from "react";

export default class AppName extends Component {
    render() {
        return <h2>Welcome to {this.props.name}</h2>;
    }
}