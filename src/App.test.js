import React from 'react';
import {shallow} from 'enzyme';
import App from './App';
import AppName from "./AppName";

it('renders without crashing', () => {
  shallow(<App />);
});

it('renders application name', () => {
  const wrapper = shallow(<App />);
  let appName = wrapper.find(AppName);
  expect(appName.length).toEqual(1);
  expect(appName.props().name).toEqual("Bookshop");
});